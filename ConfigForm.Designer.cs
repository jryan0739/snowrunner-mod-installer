﻿namespace SnowRunnerModIo
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxApiKey = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelApiKey = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxToken = new System.Windows.Forms.TextBox();
            this.labelTokenExpire = new System.Windows.Forms.Label();
            this.textBoxTokenExpire = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonGetEmailCode = new System.Windows.Forms.Button();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.buttonGetToken = new System.Windows.Forms.Button();
            this.labelToken = new System.Windows.Forms.Label();
            this.buttonForceNewToken = new System.Windows.Forms.Button();
            this.labelSelectSavegameFolder = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBoxWorkFolder = new System.Windows.Forms.TextBox();
            this.buttonSelectFolder = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSucces = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelErrorOneTimeCode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelErrorEmailCode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSucces2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelErrorOneTimeCode2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelErrorEmailCode2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.textBoxEmail, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxApiKey, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelEmail, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelApiKey, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.buttonOK, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.buttonCancel, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBoxToken, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelTokenExpire, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBoxTokenExpire, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelToken, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.buttonForceNewToken, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelSelectSavegameFolder, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // textBoxEmail
            // 
            resources.ApplyResources(this.textBoxEmail, "textBoxEmail");
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.TextChanged += new System.EventHandler(this.textBoxEmail_TextChanged);
            // 
            // textBoxApiKey
            // 
            resources.ApplyResources(this.textBoxApiKey, "textBoxApiKey");
            this.textBoxApiKey.Name = "textBoxApiKey";
            this.textBoxApiKey.TextChanged += new System.EventHandler(this.textBoxApiKey_TextChanged);
            // 
            // labelEmail
            // 
            resources.ApplyResources(this.labelEmail, "labelEmail");
            this.labelEmail.Name = "labelEmail";
            // 
            // labelApiKey
            // 
            resources.ApplyResources(this.labelApiKey, "labelApiKey");
            this.labelApiKey.Name = "labelApiKey";
            // 
            // buttonOK
            // 
            resources.ApplyResources(this.buttonOK, "buttonOK");
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // textBoxToken
            // 
            resources.ApplyResources(this.textBoxToken, "textBoxToken");
            this.textBoxToken.Name = "textBoxToken";
            this.textBoxToken.ReadOnly = true;
            // 
            // labelTokenExpire
            // 
            resources.ApplyResources(this.labelTokenExpire, "labelTokenExpire");
            this.labelTokenExpire.Name = "labelTokenExpire";
            // 
            // textBoxTokenExpire
            // 
            resources.ApplyResources(this.textBoxTokenExpire, "textBoxTokenExpire");
            this.textBoxTokenExpire.Name = "textBoxTokenExpire";
            this.textBoxTokenExpire.ReadOnly = true;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.buttonGetEmailCode);
            this.flowLayoutPanel1.Controls.Add(this.labelInstructions);
            this.flowLayoutPanel1.Controls.Add(this.textBoxCode);
            this.flowLayoutPanel1.Controls.Add(this.buttonGetToken);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // buttonGetEmailCode
            // 
            resources.ApplyResources(this.buttonGetEmailCode, "buttonGetEmailCode");
            this.buttonGetEmailCode.Name = "buttonGetEmailCode";
            this.buttonGetEmailCode.UseVisualStyleBackColor = true;
            this.buttonGetEmailCode.Click += new System.EventHandler(this.buttonGetEmailCode_Click);
            // 
            // labelInstructions
            // 
            resources.ApplyResources(this.labelInstructions, "labelInstructions");
            this.labelInstructions.Name = "labelInstructions";
            // 
            // textBoxCode
            // 
            resources.ApplyResources(this.textBoxCode, "textBoxCode");
            this.textBoxCode.Name = "textBoxCode";
            // 
            // buttonGetToken
            // 
            resources.ApplyResources(this.buttonGetToken, "buttonGetToken");
            this.buttonGetToken.Name = "buttonGetToken";
            this.buttonGetToken.UseVisualStyleBackColor = true;
            this.buttonGetToken.Click += new System.EventHandler(this.buttonGetToken_Click);
            // 
            // labelToken
            // 
            resources.ApplyResources(this.labelToken, "labelToken");
            this.labelToken.Name = "labelToken";
            // 
            // buttonForceNewToken
            // 
            resources.ApplyResources(this.buttonForceNewToken, "buttonForceNewToken");
            this.buttonForceNewToken.Name = "buttonForceNewToken";
            this.buttonForceNewToken.UseVisualStyleBackColor = true;
            this.buttonForceNewToken.Click += new System.EventHandler(this.buttonForceNewToken_Click);
            // 
            // labelSelectSavegameFolder
            // 
            resources.ApplyResources(this.labelSelectSavegameFolder, "labelSelectSavegameFolder");
            this.labelSelectSavegameFolder.Name = "labelSelectSavegameFolder";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.textBoxWorkFolder);
            this.flowLayoutPanel2.Controls.Add(this.buttonSelectFolder);
            resources.ApplyResources(this.flowLayoutPanel2, "flowLayoutPanel2");
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            // 
            // textBoxWorkFolder
            // 
            resources.ApplyResources(this.textBoxWorkFolder, "textBoxWorkFolder");
            this.textBoxWorkFolder.Name = "textBoxWorkFolder";
            // 
            // buttonSelectFolder
            // 
            resources.ApplyResources(this.buttonSelectFolder, "buttonSelectFolder");
            this.buttonSelectFolder.Name = "buttonSelectFolder";
            this.buttonSelectFolder.UseVisualStyleBackColor = true;
            this.buttonSelectFolder.Click += new System.EventHandler(this.buttonSelectFolder_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabelSucces,
            this.toolStripStatusLabelErrorOneTimeCode,
            this.toolStripStatusLabelErrorEmailCode,
            this.toolStripStatusLabelSucces2,
            this.toolStripStatusLabelErrorOneTimeCode2,
            this.toolStripStatusLabelErrorEmailCode2});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.IsLink = true;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // toolStripStatusLabelSucces
            // 
            this.toolStripStatusLabelSucces.Name = "toolStripStatusLabelSucces";
            resources.ApplyResources(this.toolStripStatusLabelSucces, "toolStripStatusLabelSucces");
            this.toolStripStatusLabelSucces.Tag = "Однократный запрос кода выполнен успешно, проверьте свою электронную почту и вста" +
    "вьте его в текстовое поле";
            // 
            // toolStripStatusLabelErrorOneTimeCode
            // 
            this.toolStripStatusLabelErrorOneTimeCode.Name = "toolStripStatusLabelErrorOneTimeCode";
            resources.ApplyResources(this.toolStripStatusLabelErrorOneTimeCode, "toolStripStatusLabelErrorOneTimeCode");
            this.toolStripStatusLabelErrorOneTimeCode.Tag = "Код состояния:";
            // 
            // toolStripStatusLabelErrorEmailCode
            // 
            this.toolStripStatusLabelErrorEmailCode.Name = "toolStripStatusLabelErrorEmailCode";
            resources.ApplyResources(this.toolStripStatusLabelErrorEmailCode, "toolStripStatusLabelErrorEmailCode");
            this.toolStripStatusLabelErrorEmailCode.Tag = "Код состояния:";
            // 
            // toolStripStatusLabelSucces2
            // 
            this.toolStripStatusLabelSucces2.Name = "toolStripStatusLabelSucces2";
            resources.ApplyResources(this.toolStripStatusLabelSucces2, "toolStripStatusLabelSucces2");
            // 
            // toolStripStatusLabelErrorOneTimeCode2
            // 
            this.toolStripStatusLabelErrorOneTimeCode2.Name = "toolStripStatusLabelErrorOneTimeCode2";
            resources.ApplyResources(this.toolStripStatusLabelErrorOneTimeCode2, "toolStripStatusLabelErrorOneTimeCode2");
            // 
            // toolStripStatusLabelErrorEmailCode2
            // 
            this.toolStripStatusLabelErrorEmailCode2.Name = "toolStripStatusLabelErrorEmailCode2";
            resources.ApplyResources(this.toolStripStatusLabelErrorEmailCode2, "toolStripStatusLabelErrorEmailCode2");
            // 
            // ConfigForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelApiKey;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        public System.Windows.Forms.TextBox textBoxEmail;
        public System.Windows.Forms.TextBox textBoxApiKey;
        private System.Windows.Forms.Button buttonGetToken;
        private System.Windows.Forms.TextBox textBoxToken;
        private System.Windows.Forms.Label labelTokenExpire;
        private System.Windows.Forms.TextBox textBoxTokenExpire;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonGetEmailCode;
        private System.Windows.Forms.Label labelInstructions;
        private System.Windows.Forms.TextBox textBoxCode;
        private System.Windows.Forms.Label labelToken;
        private System.Windows.Forms.Button buttonForceNewToken;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label labelSelectSavegameFolder;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox textBoxWorkFolder;
        private System.Windows.Forms.Button buttonSelectFolder;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSucces;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelErrorOneTimeCode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelErrorEmailCode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSucces2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelErrorOneTimeCode2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelErrorEmailCode2;
    }
}