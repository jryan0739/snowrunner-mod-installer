﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowRunnerModIo
{

    public class ModData
    {
        public List<Mod> data { get; set; }
    }

    public class ModInfo
    {
        public int visible { get; set; }
        public int id { get; set; }
        public int game_id { get; set; }
        public int date_added { get; set; }
        public int date_updated { get; set; }
        public int date_live { get; set; }
        public string name { get; set; }
        public string name_id { get; set; }
        public string summary { get; set; }

    }

    public class Mod
    {


        public Mod()
        {

        }
        public int id { get; set; }
        public string profile_url { get; set; }
        public int game_id { get; set; }
        //public string submitted_by { get; set; }
        public int date_added { get; set; }
        public int date_updated { get; set; }
        public int date_live { get; set; }
        public string name { get; set; }
        public string name_id { get; set; }
        public string summary { get; set; }
        public string description { get; set; }
        public string homepage_url { get; set; }
        public string metadata_blob { get; set; }
        //public string tags { get; set; }
        public string downloads { get; set; }
        public string popular { get; set; }
        public string rating { get; set; }
        public string subscribers { get; set; }
        public ModFile modfile { get; set; }
        public Media media { get; set; }
        [Newtonsoft.Json.JsonIgnore]
        public bool isInstalledLocal { get; set; }

        public Logo logo { get; set; }
    }

    public class Logo
    {
        public string filename { get; set; }
        public string original { get; set; }
        public string thumb_1280x720 { get; set; }
        public string thumb_320x180 { get; set; }
        public string thumb_640x360 { get; set; }
    }


    public class Media
    {
        public List<Image> images { get; set; }

        public class Image
        {

            public string filename { get; set; }
            public string original { get; set; }
            public string thumb_320x180 { get; set; }
        }
    }

    public class ModFile
    {
        public ModFile()
        {

        }
        public int id { get; set; }
        public string mod_id { get; set; }
        public string date_added { get; set; }
        public string date_scanned { get; set; }
        public string virus_status { get; set; }
        public string virus_positive { get; set; }
        public string virustotal_hash { get; set; }
        public string filesize { get; set; }
        public FileHash filehash { get; set; }
        public string filename { get; set; }
        public string version { get; set; }
        public string changelog { get; set; }
        public string metadata_blob { get; set; }
        public ModFileDownload download { get; set; }
        public string binary_url { get; set; }
        public string date_expires { get; set; }
        public class FileHash
        {
            public string md5 { get; set; }
        }

    }

    public class ModFileDownload
    {
        public ModFileDownload()
        {

        }
        public string binary_url { get; set; }
        public string date_expires { get; set; }
    }



    public class InstalledMod
    {
        public int date_updated { get; set; }
        public int mod_id { get; set; }


        public int modfile_id { get; set; }

        public bool ShouldSerializemodfile_id()
        {
            return !false;
        }
        public string path { get; set; }
    }


    public class UserProfileFile
    {
        public UserProfileFile()
        {
            this.UserProfile = new UserProfile { gdprAccept = true, gdprSeen = true, lastAgreement = 1, modStateList = new List<modStateClass>() };
            this.cfg_version = 1;
        }
        public UserProfile UserProfile { get; set; }
        public int cfg_version { get; set; }
    }

    public class UserProfile
    {
        public bool gdprAccept { get; set; }
        public bool gdprSeen { get; set; }
        public int lastAgreement { get; set; }

        public List<modStateClass> modStateList { get; set; }
    }

    public class modStateClass
    {
        public int modId { get; set; }
        public bool modState { get; set; }
    }


}
